
# Ce fichier est consacré à tester les différentes fonctions du projet



#****************************************************************** importations ************************************************************************

from scrabble.grid import generate_piece, submit_test,get_all_boxes, get_full_boxes_positions, pioche,liste_to_str
from scrabble.grid import evaluer, verification_operation, generate_empty_grid, transpose_grille
import builtins
import random as rd


#*************************************************** Définitions et initialisations des bibliothéques et constantes ************************************

stock= 4*['0']+8*['1','2','4', '5', '6', '8', '9']+3*['7']+6*['3']+15*['=']+10*['+','-']+6*['*']+4*['/']+6*['(',')']+4*['joker']



#**************************************************** definitions des fonctions ************************************************************************

# tester la fonction generate_piece
def test_generate_piece():
    assert generate_piece() in stock




hand = ["1","2","4"]


#tester la fonction test_pioche 


def test_pioche():
    assert pioche() == 7



#tester la fonction submit_test

def test_submit_test(monkeypatch):
    def mock_input(input):
       a = rd.random()
       if a<0.5 :
           return "y"
       else :
           return "n"
    monkeypatch.setattr(builtins,'input', mock_input)
    x = submit_test()
    assert x ==False or x==True


# tester la fonction get_all_boxes


def test_get_all_boxes():
    assert get_all_boxes([['1',' ',' '],[' ','2',' '],[' ','3','4']]) == ['1',' ',' ',' ','2',' ',' ','3','4']


# tester la fonction get_full_boxes_positions


def test_get_full_boxes_positions():
    assert get_full_boxes_positions([['1',' ',' '],[' ','2',' '],[' ','3','4']]) == [[0,0],[1,1],[2,1],[2,2]]


# tester la fonction test_liste_to_str


def test_liste_to_str():
    assert liste_to_str(["0","2","9","+"])=="029+"



# tester la fonction evaluer


def test_evaluer():
    assert evaluer("057s") == -1
    assert evaluer("057(8") == -1
    assert evaluer("025") == 25
    assert evaluer("206") == 206
    assert evaluer("24+06") == 30
    assert evaluer ("2*(3+4)") == 14
    assert evaluer ("26(6)") == -1
    assert evaluer ("26+(6)") == 32
    assert evaluer ("+5") == -1
    assert evaluer ("-5") == -1
    assert evaluer ("65+") == -1
    assert evaluer ("+645+54+9851") == -1
    assert evaluer ("5+(-4)") == -1
    assert evaluer("5+(+4)") == -1
    assert evaluer("(56)") == 56
    assert evaluer("5+*6") == -1
    assert evaluer("5*+6") == -1
    assert evaluer("5+(8+(7))") == -1
    assert evaluer("5+(2+3)6") == -1




#tester la fonction verification_operation 



def test_verification_operation():
    assert verification_operation("5+2+3") == False
    assert verification_operation("5+8+7=") == False
    assert verification_operation("5+2=7=8-1=9-2") == False
    assert verification_operation("5+2=7=6+1") == True
    assert verification_operation("+6=+5+1") == False
    assert verification_operation("6=5+1") == True
    assert verification_operation("-1=+6") == False
    assert verification_operation(("56=(56)")) == True



#tester la fonction generate_empty_grid



def test_generate_empty_grid():
    assert generate_empty_grid(n=3)==[[' ',' ',' '],[' ','=',' '],[' ',' ',' ']]

def test_transpose_grille():
    assert transpose_grille([['1','2','3'],['2','4','6'],['3','6','9']]) == [['1','2','3'],['2','4','6'],['3','6','9']]
    assert transpose_grille([['1','8','3'],['2','4','8963465'],['2','6','9']]) == [['1','2','2'],['8','4','6'],['3','8963465','9']]
