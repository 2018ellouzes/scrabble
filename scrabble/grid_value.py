
#Ce fichier génère grid_valeus 


#********************************************************** Definition des fonctions *****************************************************************

def init_val_grid():
    
    """ Cette fonction permet de générer grid_values et de la retourner ,
    C'est une grille de 15*15  qui défnira la valeur de chaque case et qui ne sera pas modifiée au cours du jeu """
    
    
    grid_values=[]
    for i in range(15):
        grid_values.append(["normal"]*15)

#  Definition des cases à valeur 2W
    for i in range(15):
        grid_values[i][i]="2W"
        grid_values[14-i][i]="2W"
    print(grid_values)

# Definition des cases à valeur 3W
    grid_values[0][7]="3W"
    grid_values[7][0]="3W"
    grid_values[14][7]="3W"
    grid_values[7][14]="3W"

    grid_values[0][0]="3W"
    grid_values[14][14]="3W"
    grid_values[0][14]="3W"
    grid_values[14][0]="3W"

# Definition des cases à valeur 3L
    L=[1,5,9,13]
    for i in L:
        for j in L:
            grid_values[i][j]="3L"

# Redefinition des cases 2W modifiées
    grid_values[1][1]="2W"
    grid_values[13][13]="2W"
    grid_values[1][13]="2W"
    grid_values[13][1]="2W"

# Definition des cases à valeur 3W
    grid_values[0][3]="2L"
    grid_values[0][11]="2L"
    grid_values[14][3]="2L"
    grid_values[14][11]="2L"
    grid_values[3][0]="2L"
    grid_values[11][0]="2L"
    grid_values[3][14]="2L"
    grid_values[11][14]="2L"

    grid_values[2][8]="2L"
    grid_values[2][6]="2L"
    grid_values[3][7]="2L"

    grid_values[12][8]="2L"
    grid_values[12][6]="2L"
    grid_values[11][7]="2L"

    grid_values[7][3]="2L"
    grid_values[7][11]="2L"

    for i in [6,8]:
        for j in [2,6,8,12]:
            grid_values[i][j]="2L"
    print(grid_values)
    return(grid_values)

