
Le projet consiste à élaborer un jeu de scrabbles à base de chiffres et opérateurs mathématiques .
La répartition des taches et les différentes étapes du projet ont été définis en termes de sprints et fonctionnalités en tenant à passer par une MVP 
comme expliqué ci-dessous: 



Sprint 0 :

Choix du projet : Jeu de scrabble mathématiques.
Réflexion autour des règles du jeu, de la conception, choix  d’un vocabulaire commun.


Sprint 1 : Mise en place des données du jeu

Fonctionnalité 1 : Initialisation des différents dictionnaires et différentes grilles, des valeurs par défaut.
Fonctionnalité 2 : Afficher une grille de jeu, initialiser une grille avec des valeurs aléatoirement positionnées.

Sprint 2 : Actions des joueurs et évaluation du tour

Fonctionnalité 3 : Récupérer les instructions du joueur.
Fonctionnalité 4 : Evaluation de l’expression choisie par le joueur.
Fonctionnalité 5 : Calculer le score en cas d’une expression valide


Sprint 3 : Jouer !

Fonctionnalité 6 : Faire jouer un joueur et mettre à jour la grille 


Sprint4 : Interface graphique 

Fonctionnalité 8 : Elaboration d’une interface graphique en tenant compte des différentes règles de jeu.
Fonctionnalité 9 : Mise en relation avec la version comsol. 

Sprint5 : Perfectionnement

Fonctionnalité 10 :Etude des différentes exceptions 
Fonctionnalité 11 : Tester toutes les étapes du jeu 
