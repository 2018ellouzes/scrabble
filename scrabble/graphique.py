
#Ce fichier élabore la version graphique du jeu 

#*********************************************************** Importations ****************************************************************************
from tkinter import *
from scrabble.grid import  *
from scrabble.grid_value import *

#********************************************************* Initialisations ***************************************************************************


# Un dictionnaire définissant les couleurs pour chaque type de case de la grille 

TILES_BG_COLOR={"normal":"#00420F","3W":"#DC0723","2L":"#8AD885","2W":"#FAB3B3","3L":"#85B4D8"} 

# initialisation du score à 0

score = 0 

joker = -1

# initilaisation de grid_value 

grid_value = init_val_grid()


x,y = -1,-1

# initialisation de la main 
hand = []
pioche(hand)
operation=[]
grid_Labels =[]

game = Tk() # C'est la fenetre principale de la version graphique


grid_graphic = Canvas (game,width=300,height=300)

# generation d'une grille avec des cases aléatoires 

grid = generate_grid_with_random_elements()

hand_Labels = []
hand_graphic = Canvas(game,width = 300 , height = 200) # visualisation de la main 
hand_a_afficher = hand.copy()



def initialiser_grid():
    
    """ Initialisation de la grille"""
    
    global grid_graphic
    global  grid_Labels
    global grid
    for i in range (15):
        for j in range(15):
            if grid[i][j] == " ":
                l = grid_graphic.create_rectangle (20*j,20*i,20*j+20,20*i+20,width = 1, fill = TILES_BG_COLOR[grid_value[i][j]])


            else :
                l = grid_graphic.create_rectangle (20*j,20*i,20*j+20,20*i+20,width = 1, fill = '#e2ecc3')
            c = grid_graphic.create_text(20*j+10,20*i+10,text = grid[i][j])
            grid_Labels.append([l,c])

def initialiser_hand ():
    
    """ Initialsiation de la main  """
    
    global score
    global hand_a_afficher
    global  hand_Labels
    for i in range(7):
        l = hand_graphic.create_rectangle (20*i,20,20*i+20,40,width = 1, fill = '#e2ecc3')
        c = hand_graphic.create_text(20*i+10,20+10,text = hand[i])

        hand_Labels.append([l,c])
    l = hand_graphic.create_rectangle (180,20,220,40,width = 1, fill = '#F0C3D7')
    c = hand_graphic.create_text(200,20+10,text = "↓")
    l = hand_graphic.create_rectangle (230,20,270,40,width = 1, fill = '#F0C3D7')
    c = hand_graphic.create_text(250,20+10,text = "→")
    l = hand_graphic.create_rectangle (280,20,300,40,width = 1, fill = '#F0C3D7')
    c = hand_graphic.create_text(290,20+10,text = "◄")
    sc = hand_graphic.create_text(150,70,text = "score: " + str(score))
    hand_Labels.append(sc)

def configure_hand(hand):
    global hand_Labels
    for i in range(7):
        hand_graphic.itemconfigure (hand_Labels[i][1],text = hand[i])
    hand_graphic.itemconfigure (hand_Labels[7],text = "score: " + str(score))

def configure_grid():
    global  grid_Labels
    global grid
    for i in range (15):
        for j in range(15):
            grid_graphic.itemconfigure (grid_Labels[15*i+j][1],text = grid[i][j])
            if grid[i][j] != " ":
                l = grid_graphic.itemconfig (grid_Labels[15*i+j][0], fill = '#e2ecc3')



def choisir_item(event):
    global grid
    global hand_a_afficher,hand
    global operation
    global x,y,score,joker
    global etat1,etat2
    grid_copy = grid.copy()
    if event.x < 140:
        print(hand_a_afficher)
        if hand_a_afficher[event.x //20] !=" " and hand_a_afficher[event.x//20] != "joker" :
            operation.append(hand_a_afficher[event.x//20])
            hand_a_afficher[event.x//20]=" "
            configure_hand(hand_a_afficher)
            hand_graphic.grid(row=1,column=0,columnspan=6)
            print(operation)
        elif hand_a_afficher[event.x // 20] == "joker" :
            Joker.grid(row=3,column = 0 ,columnspan=6)
            game.mainloop()
            operation.append(str(joker))
            print(joker)
            hand_a_afficher[event.x//20]=" "
            joker = -1
            configure_hand(hand_a_afficher)
            hand_graphic.grid(row=1,column=0,columnspan=6)

    elif  event.x in range(180,220) :
        direction = "bas"
        print(operation)
        xi, yi, grid,formule = position(x,y,direction,operation,grid)
        configure_grid()
        grid_graphic.grid(row = 0,columnspan = 6)
        etat1 = True
        etat2 = False
        print(hand_a_afficher)
        if grid != grid_copy:
            score = score + evaluate(xi,yi,direction,formule,grid_copy)
            print(score)
            while True:
                try :
                    hand_a_afficher.remove(" ")
                except :
                    break
            hand = hand_a_afficher.copy()
            pioche(hand)
            hand_a_afficher = hand.copy()
            operation = []
            configure_hand(hand_a_afficher)
            hand_graphic.grid(row=1,column = 0 , columnspan = 6)
            hand_graphic.unbind("<Button-1>")
            game.quit()
        else :
            hand_a_afficher = hand.copy()
            operation = []
            configure_hand(hand_a_afficher)
            hand_graphic.grid(row=1,column= 0 ,columnspan = 6)
            hand_graphic.unbind("<Button-1>")
            game.quit()
    elif  event.x in range(230,270) :
        direction = "droite"
        print(operation)
        xi, yi,grid,formule = position(x,y,direction,operation,grid)
        configure_grid()
        grid_graphic.grid(row = 0,column=0,columnspan=6)
        etat1 = True
        etat2 = False
        if grid != grid_copy:
            score = score + evaluate(xi,yi,direction,formule,grid_copy)
            while True:
                try :
                    hand_a_afficher.remove(" ")
                except :
                    break
            hand = hand_a_afficher.copy()
            pioche(hand)
            hand_a_afficher = hand.copy()
            operation = []
            configure_hand(hand_a_afficher)
            hand_graphic.grid(row=1,column = 0 , columnspan = 6)
            hand_graphic.unbind("<Button-1>")
            game.quit()
        else :
            hand_a_afficher = hand.copy()
            operation = []
            configure_hand(hand_a_afficher)
            hand_graphic.grid(row=1,column = 0 , columnspan = 6)
            hand_graphic.unbind("<Button-1>")
            game.quit()
    elif event.x in range(280,300):
        print(operation)
        change_pieces(hand,operation)
        hand_a_afficher = hand.copy()
        operation = []
        configure_hand(hand_a_afficher)
        hand_graphic.grid(row=1,column = 0 , columnspan = 6)
        hand_graphic.unbind("<Button-1>")
        game.quit()
        etat1 = True
        etat2 = False


def case_depart(event):
    """ Fonction permettant de sélectionner la case de départ"""
    
    global grid_graphic
    global etat1,etat2
    global x,y
    x,y = event.y//20,event.x//20
    print(x,y)
    etat1 = False
    etat2 = True
    # grid_graphic.quit()
    grid_graphic.unbind("<Button-1>")
    print(etat1,etat2)
    game.quit()
    # print (event.y)


def instruction():
    
    """ Cette fonction permet de générer une fenetre d'instructions """
    def generate_space(background,height=50):
        space=Frame(background, bg="#DCFEEA",height= height , width=500)
        space.grid()





    instruction=Toplevel(game)
    instruction.title("Game Instructions")




    background=Frame(instruction,height= 500, width= 500,bg="#DCFEEA")
    Title=Label(background, bg="#DCFEEA", text="Les instructions du jeu ", fg="black", font=('Calibri',30,"bold"))

    text="""Ce jeu est similaire à un jeu de Scrabble ordinaire sauf qu'il met en jeu plutot
    des CHIFFRES et des OPERATEURS mathématqiues .
    Votre main comportera 7 pièces depuis le stock que vous pourrez permuter avec des pièces du stock.
    Un tour consiste à choisir les pièces à jouer avec depuis sa main, la case de départ 
    de votre expression et la direction de dépot
    deux directions sont possibles : vers le bas ou à droite 
    La grille sera initilisée avec des cases placées aléatoirement et à vous 
    de la compléter avec le plus de pièces possibles.
    
    """

    Text=Label(background, bg="#DCFEEA", text=text, fg="black", font=('Calibri',15))



    background.grid()
    generate_space(background,10)
    Title.grid()
    generate_space(background)
    Text.grid()

    generate_space(background)



    keys=Frame(background, bg="#DCFEEA" ,height= 300, width=500)

    cube_N=Frame(keys, bg="#00420F", height=30 , width=30, relief= "solid",bd=2)
    cube_label_N= Label(keys, bg= "#DCFEEA", text= "Une case normale ",font=("Arial",10,"bold") )

    space=Frame(keys, bg="#DCFEEA" , width=100, height=100)
    space_text=Label(space,bg="#DCFEEA", text=" Le code des couleurs \n des cases  ", font=("Calibri",18,"bold","underline"))



    cube_2W=Frame(keys, bg="#FAB3B3", height=30 , width=30, relief= "solid",bd=2)
    cube_label_2W= Label(keys, bg= "#DCFEEA", text= """ Cette case multiplie \n ton mot par 2 """,font=("Arial",10,"bold") )

    keys.grid()
    cube_N.grid(column=0, row=0)
    cube_label_N.grid(column=0 , row =1)

    space.grid(column=1, row=0)
    space_text.grid()

    cube_2W.grid(column=2, row=0)
    cube_label_2W.grid(column=2 , row =1)

    separator=Frame(keys, bg="#DCFEEA", height=50)
    separator.grid(row=3)

    cube_3W=Frame(keys, bg="#DC0723", height=30 , width=30, relief= "solid",bd=2)
    cube_label_3W= Label(keys, bg= "#DCFEEA", text= " Cette case multiplie \n ton mot par 3 ",font=("Arial",10,"bold") )

    cube_3L=Frame(keys, bg="#85B4D8", height=30 , width=30, relief= "solid",bd=2)
    cube_label_3L= Label(keys, bg= "#DCFEEA", text= " Cette case multiplie  \n ta lettre par  3 ",font=("Arial",10,"bold") )

    cube_2L=Frame(keys, bg="#8AD885", height=30 , width=30, relief= "solid",bd=2)
    cube_label_2L= Label(keys, bg= "#DCFEEA", text= " Cette case multiplie \n ta lettre par 2 ",font=("Arial",10,"bold") )


    cube_3W.grid(column=0, row=4)
    cube_label_3W.grid(column=0 , row =5)

    cube_3L.grid(column=1, row=4)
    cube_label_3L.grid(column=1, row =5)

    cube_2L.grid(column=2, row=4)
    cube_label_2L.grid(column=2 , row =5)
    generate_space(background,100)

def charger_partie():
    global grid,hand,score,grid_Labels,hand_a_afficher
    fichier = open("sauv.txt","r").read()
    score = int(fichier[fichier.find("%")+1:fichier.find("#")])
    hand = list(fichier[fichier.find("/")+1:fichier.find("%")])
    while True:
        try:
            hand.remove("'")
        except :
            break
    while True:
        try:
            hand.remove(" ")
        except :
            break
    while True:
        try:
            hand.remove("[")
            hand.remove("]")
        except :
            break
    while True:
        try:
            hand.remove(",")
        except :
            break
    stock = list(fichier[fichier.find("#")+1:])
    while True:
        try:
            stock.remove("'")
        except :
            break
    while True:
        try:
            stock.remove(",")
        except :
            break
    while True:
        try:
            stock.remove(" ")
        except :
            break
    while True:
        try:
            stock.remove("[")
            stock.remove("]")
        except :
            break
    grid =[]
    nombre =""
    grid_str = fichier[1:fichier.find("/")]
    for i in range(14):
        print(grid_str[15*i:15*i+15])
    ligne = []
    for i in range (len(grid_str)-1):
        if grid_str[i] == "[":
            ligne.append(grid_str[i+2])
        elif grid_str[i] == "," and grid_str[i+2] !="[":
            ligne.append(grid_str[i+3])
        elif grid_str[i] == "]" :
            grid.append(ligne)
            ligne =[]


    print(hand,stock,score)
    operation =[]
    hand_a_afficher = hand.copy()
    modify_stock(stock)
    grid_Labels =[]
    initialiser_grid()
    configure_hand(hand)
    hand_graphic.grid(row=2)
    grid_graphic.grid()



def rejouer () :
    global grid,grid_graphic,grid_Labels,hand_Labels,hand_graphic,hand,hand_a_afficher,operation,score
    grid = generate_grid_with_random_elements()
    initiate_stock()
    hand = []
    pioche(hand)
    hand_a_afficher = hand.copy()
    grid_Labels =[]
    score = 0
    initialiser_grid()
    configure_hand(hand_a_afficher)
    grid_graphic.grid(row = 0,column = 0 , columnspan = 6)
    hand_graphic.grid(row=1,column = 0 , columnspan = 6)
    # print(grid)
    etat1 = True
    etat2 = False
    operation=[]

def sauvegarder_partie():
    global grid,hand,score
    stock = get_stock()
    fichier = open("sauv.txt", "w")
    fichier.write(str(grid) + "/" +  str(hand) + "%" + str(score) + "#" + str(stock))
    fichier.close()

def get_value_joker():
    global Joker,valeur_joker,joker
    joker = valeur_joker.get()
    Joker.grid_remove()
    game.quit()
Joker = Frame(game)
Label(Joker,text="choisir valeur joker").grid(column = 0)
valeur_joker = Spinbox(Joker,from_=0,to=9)
valeur_joker.grid(column =1,row=0)
Button(Joker,text="valider", command = get_value_joker).grid(column = 2,row=0)
bouton_rejour = Button(game, command = rejouer,text = "rejouer")
bouton_rejour.grid(row = 4,column=0)
bouton_instruction = Button(game,command = instruction,text="instructions")
bouton_instruction.grid(row=4,column =3)
bouton_sauvegarder_partie = Button(game,command = sauvegarder_partie,text="sauvegarder")
bouton_sauvegarder_partie.grid(row = 4,column = 2)
bouton_recharger_partie = Button(game,command = charger_partie,text = "charger partie")
bouton_recharger_partie.grid(row = 4,column = 1)
initialiser_grid()
initialiser_hand()
grid_graphic.grid(row = 0,column =0,columnspan = 6)
hand_graphic.grid(row=1, column = 0 ,columnspan = 6)
etat1 = True
etat2 = False
print(len(grid_Labels))

while True:
    print("a")
    if etat1 :
        grid_graphic.bind("<Button-1>",case_depart)


        game.grid()
        game.mainloop()
    print(etat2,etat1)
    if etat2 :
        print("dd")
        hand_graphic.bind("<Button-1>",choisir_item)
        game.grid()
        game.mainloop()
game.mainloop()

print("aa")
