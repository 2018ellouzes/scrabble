from tkinter import*
from random import *
from tkinter import messagebox
import itertools as it
stock = ["0"]*4 + ["1","2","4","5","6","8","9"]*8 + ["3"]*6 + ["7"]*3 + ["+","-"]*10 + ["/"]*4 + ["*"]*6 + ["(",")"]*6 + ["="] * 15 + ["joker"]*4
val = {"0":1,"1":1,"2":1,"3":2,"4":1,"5":1,"6":1,"7":4,"8":1,"9":1,"+":1,"-":1,"*":3,"/":4,"=":1,"(":2,")":2}
hand = []
hand_mask = [False]*7
ecriture=("comic sans ms",13)
nbcase=15
case=32
x0,y0=9,9
fenetre=Tk()
fenetre.configure(bg = "brown")
Cadre=Frame(fenetre)
Texte1=Label(fenetre,text="Scrabmaths",fg="red",font=ecriture)
TexteC=Text(fenetre,bg="orange",height=25,width=30)
Can=Canvas(Cadre,height=500,width=500)
def generate_piece():
    global stock
    i=randint(0,len(stock)-1)
    piece=stock.pop(i)
    return piece
def fin ():
    fenetre.quit()
    fenetre.destroy()
cases =dict()
def grille():
    for i in range(nbcase+1):
        Can.create_line(x0+case*i, y0,x0+case*i,y0 + nbcase*case)
        Can.create_line(x0, y0+case*i,x0+nbcase*case ,y0+case*i)
    for r in range(nbcase):
        x = x0 + case * r + case // 2
        for c in range(nbcase):
            y = y0 + case * c + case // 2
            cases[(r, c)] = Can.create_text(x, y, text=' ')
def donne_position(event):
    i,j = jouer(event)
    TexteC.delete("0.0",END)# on efface l'écriture précédente
    TexteC.insert(END,"clic detecté en x="+str(i+1) + " et y = " + str(j+1))
ma_case=[]
def jouer(event):
    global ma_case
    [i,j]=correspond(event.x,event.y)
    if i in range(nbcase) and j in range (nbcase):   # on ne fait rien si le click est hors grille
        #Can.create_rectangle(x0 +c*j,y0+c*i,x0 +c*(j+1),y0+c*(i+1),fill=coul(i,j))
        ma_case = [i,j]
        return i,j
def correspond(x,y):
    return [(y-y0)//case,(x-x0)//case]
#def valider(event):
#def saisir(event):
def pioche():
    global hand
    for i in range(7-len(hand)):
        car = generate_piece()
        bouton_car = Button(fenetre, text=car, command=lambda arg =i : chiffre(arg))
        bouton_car.grid(row = 2, column = i+10)
direction = ""
def directionh():
    global direction
    #direc_dict = {"↑":"haut","↓":"bas","←":"gauche","→":"droite"}
    direction = "haut"
    print(direction)
def directionb():
    global direction
    #direc_dict = {"↑":"haut","↓":"bas","←":"gauche","→":"droite"}
    direction = "bas"
    print(direction)
def directiong():
    global direction
    #direc_dict = {"↑":"haut","↓":"bas","←":"gauche","→":"droite"}
    direction = "gauche"
    print(direction)
def directiond():
    global direction
    #direc_dict = {"↑":"haut","↓":"bas","←":"gauche","→":"droite"}
    direction = "droite"
    print(direction)
Texte1.grid(row=0,column=0)
Cadre.grid(row=1,column=0)
Can.grid(row=2, column=0)
TexteC.grid(row=1, column=3)
Can.bind("<Button-1>",donne_position)
#Can.bind( '<KeyPress>', jouer)
Can.focus_set()
bouton_droit = Button(fenetre, text="→", command=directiond,height=5,width=5)
bouton_gauche = Button(fenetre, text="←", command=directiong,height=5,width=5)
bouton_bas = Button(fenetre, text="↓", command=directionb,height=5,width=5)
bouton_haut = Button(fenetre, text="↑", command=directionh,height=5,width=5)
bouton_pioche = Button(fenetre, text="pioche", command=pioche,height=5,width=10)
bouton_quitter = Button(fenetre, text="Quitter", command=fenetre.quit,height=5,width=10)
bouton_valider = Button(fenetre, text="valider", command=fenetre.quit,height=5,width=10)
BouttonJouer=Button(fenetre,text="jouer", command=grille,height=5,width=10)
bouton_quitter.grid(row = 10, column = 1)
bouton_pioche.grid(row = 10, column = 2)
bouton_valider.grid(row = 10, column = 3)
bouton_droit.grid(row = 11, column = 12)
bouton_gauche.grid(row = 11, column = 10)
bouton_haut.grid(row = 10, column = 11)
bouton_bas.grid(row = 11, column = 11)
BouttonJouer.grid(row=0, column=50)

