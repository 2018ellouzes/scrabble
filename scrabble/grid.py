
#********************************************************************** Importations ********************************************************************

from random import *
from copy import *
from scrabble.grid_value import *


#**************************************************************** Definition des dictionnaires et constantes ********************************************

stock = ['0', '0', '0', '0', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '3', '3', '3', '3', '3', '3', '7', '7', '7', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '/', '/', '/', '/', '*', '*', '*', '*', '*', '*', '=', '+', '=', '+', '=', '-', '=', '-', '=', '-', '=', '+', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', 'joker', 'joker', 'joker', 'joker']
val ={'0': 1, '1': 1, '2': 1, '3': 2, '4': 1, '5': 1, '6': 1, '7': 4, '8': 1, '9': 1, '+': 1, '-': 1, '*': 3, '/': 4, '=': 1, '(': 2, ')': 2}


#********************************************************************************************************************************************************



# initialisation de la variable grid_values 


grid_values = init_val_grid()



def generate_piece():
    
    """ Cette fonction permet de générer une pièce aléatoire depuis le stock  """
    global stock
    i=randint(0,len(stock)-1)
    piece=stock.pop(i)
    return(piece)



def change_pieces(hand,pieces):
    
    """ Cette fonction permet de remplacer des pièces choisies de la main par des nouvelles du stock """
    global stock
    for i in pieces :
        stock.append(i)
        hand.remove(i)
        hand.append(generate_piece())





def evaluer(operation):
    
    """ Cette fonction permet de donner la valeur d'une opération mathématique 
    Elle retourne -1 si l'écriture mathématique de cette opération est fausse"""
    
    operation_copy= operation
    i = 0
    if operation_copy == "":
        return(-1)
    while i < len(operation_copy) :

        if operation_copy[i] not in ["(",")","+","-","/","*","0","1","2","3","4","5","6","7","9","8"," "]:
            return(-1) #pièce non existante

        if operation_copy[0] == "0" and i < len(operation_copy) -1 :
            if operation_copy[1] in ["1","2","3","4","5","6","7","9","8"] :
                operation_copy = operation_copy[1:]
        elif operation_copy[i] in ["(",")","+","-","/","*"] and i<len(operation_copy)-1 :
            if operation_copy[i+1] == "0" and i+1 < len(operation_copy) -1 :
                if operation_copy[i+2] in ["1","2","3","4","5","6","7","9","8"] :
                    operation_copy = operation_copy[:i+1] + " " + operation_copy[i+2:]
        if operation_copy[i] in ["+","-","/","*"] and i<len(operation_copy)-1:
            if operation_copy[i+1] in ["+","-","/","*"] :
                return (-1)
        if operation_copy[0] in ["+","-"]:
            return (-1)
        if operation_copy[i] == "(":
            if operation_copy[i+1] in ["+","-"]:
                return (-1)
        if operation_copy[i] == "(" and i>0:
            if operation_copy[i-1] not in ["+","-","/","*"] :
                return -1
            k = i+1
            while (k<len(operation_copy)) and (operation_copy[k] != ")"):
                if operation_copy[k] == "(":
                    return (-1)

                k = k + 1
            if k < len(operation_copy)-1 :
                if operation_copy[k+1] not in ["+","-","/","*"] :
                    return (-1)
            if k == len(operation_copy) :
                return(-1)
        i = i + 1
    try :
        return(eval(operation_copy))
    except :
        return (-1)






def verification_operation(operation):
    """ Cette fonction permet de vérifier qu'une égalité mathématique est bien écrite et qu'elle est mathématiquement correcte.
    Elle retourne un booléen"""
    
    b = True
    nombres =[]
    if operation.find('=') ==-1 or operation.find('=') == len(operation)-1 :
        return(False)
    elif operation[operation.find('=')+1:].find('=') != -1 :

        h = operation.find("=")
        m = operation[h+1:]

        f = m.find('=') + h + 1
        if operation[f+1:].find("=") != -1 :
            return(False)
        else :
            b = evaluer(operation[0:h]) == evaluer(operation[h+1:f]) and evaluer(operation[h+1:f]) + evaluer(operation[0:h])  != -2

            b = b and evaluer(operation[0:h]) == evaluer(operation[f+1:]) and evaluer(operation[f+1:]) + evaluer(operation[0:h])  != -2
            return(b)
    else :
        return(evaluer(operation[0:operation.find('=')]) == evaluer(operation[operation.find('=')+1:]) and evaluer(operation[operation.find('=')+1:]) + evaluer(operation[0:operation.find('=')])  != -2)


def pioche (hand):
    
    """ Cette fonction permet de mettre à jour la main du joueur pour en etre toujours à 7 pièces """
   while len(hand)<7 :
       hand.append(generate_piece())


def submit_test():
    
    """ Cette fonction permet au joueur de choisir de soumettre ou pas son choix des pièces pour  passer à l'étape suivante du jeu   """
    while True:
        test=input("Would you like to submit your operation ? Y / N ")
        if test.upper() in ["Y","N"]:
            return(test.upper()== "Y")

def select_piece(hand):
    
    """ Cette fonction demande au joueur de choisir une pièce de sa main.
    Elle retourne la pièce choisie """

    print(hand)
    piece=input("select a piece from your hand  ")
    if piece in hand :
        hand.remove(str(piece))
    return(piece)


def check_line_column():
    
    """ Cette fonction demande au joueur de choisir une case de départ de la grille .
    Elle retourne les coordonnées de cette case"""

    bool= False
    while not bool:
        i=input("entrez votre absisse")
        j=input("entrez votre ordonnée")
        bool= i in range(15) and j in range(15)
    return(i,j)

def check_command():
    
    """ Cette fonction demande au joueur de choisir une direction parmi les deux directions possibles "bas" ou "droit" pour déposer les pièces choisies """
    bool= False
    while not bool:
        move=input("entrez votre direction (bas ou droit)")
        bool= move=='bas' or move=='droit'
    return move

def get_all_boxes(tableau):
    
    """ Cette fonction retourne une liste contenant toutes les valeurs contenues dans la grille  """
    
   n=len(tableau)
   l=[]
   for i in range(n):
       for j in range(n):
               l.append(tableau[i][j])
   return l


def get_full_boxes_positions(tableau):
    
    """ Cette fonction retourne une liste contenant les coordonnées de toutes les cases non vides de la grille """
    
   n=len(tableau)
   l=get_all_boxes(tableau)
   full=[]
   m=len(l)
   for i in range(m):
       if l[i]!=' ':
           full.append([i//n,i%n])
   return(full)



movepool=['0','1','2','3','4','5','6','7','8','9']
operationpool=['+','-','*','/','=']



def generate_grid_with_random_elements(n=15):
    
    """ Cette fonction permet de générer une grille avec des pièces positionnées aléatoirement"""
    
    grid=[[' ' for i in range(n)] for j in range(n)]
    grid[7][7]='='
    all_coordinates=[]
    for i in range(n):
        for j in range(n):
            all_coordinates.append([i,j])
    all_coordinates.remove([7,7])
    value=choice(movepool)
    coordinates=choice(all_coordinates)
    while len(get_full_boxes_positions(grid))<14:
            grid[coordinates[0]][coordinates[1]]=value
            all_coordinates.remove(coordinates)
            value=choice(movepool)
            coordinates=choice(all_coordinates)
    return grid
    
    
    
def generate_empty_grid(n=15):
    """ Cette fonction permet de générer une grille vide de 15*15 """
    l=[[' ' for i in range(n)] for j in range(n)]
    l[n//2][n//2]='='
    return(l)



def evaluate(x,y,direction,expression,grid):
    
    """ Cette fonction permet à partir de la donnée des coordonnées de la case de départ , de la direction de dépot , des pièces choisises et de la grille 
    de donner le score de l'expression choisie en supposant que cette dernière est correcte """
    
    # C'est une grille de 15*15 qui ne sera pas modifiée lors du jeu et qui permet d'indexer les cases spéciales de la grille
    # Attention ici il faut voir si la grille elle va etre modifiée ou pas avant de faire l'appel de la fonction
    # La fonction présuppose que la grille elle n 'est pas encore modifiée et que l'expression est bien valide
    global grid_values
    c=0 # Cest un compteur qui permet de parcourir l'expression = les pieces chosisies par le joueur dans l'ordre de choix
    value=0
    multiplie_word=1
    if direction=="bas":
        while c < len(expression) and x < len(grid):
            if grid[x][y] ==" ":
                piece= expression[c]
                c=c+1
            else:
                piece=grid[x][y]
            value,multiplie_word=value+evaluate_case(x,y,piece,multiplie_word)[0],evaluate_case(x,y,piece,multiplie_word)[1]
            x=x+1
            print(value)
        try :
            while grid[x][y] != " ":
                piece=grid[x][y]
                value,multiplie_word=value+evaluate_case(x,y,piece,multiplie_word)[0],evaluate_case(x,y,piece,multiplie_word)[1]
                x=x+1
                # print(value)
        except :
            None

        value=value*multiplie_word
        return(value)

    if direction=="droite":
        while c < len(expression) and y < len(grid):
            print(y,c)
            if grid[x][y] ==" ":
                piece= expression[c]
                c=c+1
            else:
                piece=grid[x][y]
            value,multiplie_word=value+evaluate_case(x,y,piece,multiplie_word)[0],evaluate_case(x,y,piece,multiplie_word)[1]
            y=y+1
        try :
            while grid[x][y] != " ":
                piece=grid[x][y]
                value,multiplie_word=value+evaluate_case(x,y,piece,multiplie_word)[0],evaluate_case(x,y,piece,multiplie_word)[1]
                y=y+1
        except:
            None
        value=value*multiplie_word
        return(value)









def evaluate_case(x,y,piece,multiplie_word):
    
    """ Cette fonction permet de retourner la valeur d'une pièce suivant son emplacement sur la grille
    Elle retourne la valeur de la pièce et met à jour la variable multiplie_word essentielle à la définition du score final"""
    
    # piece retourne la valuer de la pièce c'est une chaine de caractere
    global grid_values

    if grid_values[x][y]=="normal":
        return(val[piece],multiplie_word)
    if grid_values[x][y]=="2L":
        return(val[piece],multiplie_word)
    if grid_values[x][y]=="3L":
        return(val[piece],multiplie_word)
    if grid_values[x][y]=="2W":
        multiplie_word=multiplie_word*2
        return(val[piece],multiplie_word)
    if grid_values[x][y]=="3W":
        multiplie_word=multiplie_word*3
        return(val[piece],multiplie_word)


def transpose_grille(grid):
    """ Cette fonction retourne la transposée d'une grille  """
    grid_transpose =[]
    for i in range(len(grid)):
        ligne = []
        for j in range(len(grid)):
            ligne.append(grid[j][i])
        grid_transpose.append(ligne)
    return(grid_transpose)

def neighbour(grid, x,y,valeur,direction):
    
    """  Cette fonction permet de vérifier si il y a des cases adjacents aux cases à utiliser lors d'un tour ,
    elle permet de respecter les règles du jeu de scrabble. """
    
    if direction == "bas":
        if y != 0 and y!= len(grid) - 1 :
            return(grid[x][y-1] == " " and grid[x][y+1] == " ")
        elif y == 0:
            return(grid [x][y+1] == " ")
        elif y == len(grid) -1 :
            return(grid[x][y-1] == " ")
    elif direction == "droite":
        if x != 0 and x!= len(grid) - 1 :
            return(grid[x-1][y] == " " and grid[x+1][y] == " ")
        elif x == 0:
            return(grid [x+1][y] == " ")
        elif x == len(grid) -1 :
            return(grid[x-1][y] == " ")






def position(x,y,direction,liste,grid):
    
    """ Cette fonction permet de mettre à jour la grille suite à un tour de jeu  """
    
    joker_position=[]
    for i in range(len(liste)):
        if liste[i]=='joker':
            joker_position.append(i)
    for c in joker_position:
        liste[c]=input('que voulez vous que votre joker prenne comme valeur?')
    # global grid
    back_up = deepcopy(grid)
    non_empties_coordinates = get_full_boxes_positions(back_up)
    formule = ""
    position_valide = False # vérifie si au moins l'une des cases déjà présentes va être utilisée
    neighbours = True  # renvoie si la case est adjacente à un autre case
    if direction == "bas":
        empty_tiles = 0
        for i in range(x,len(grid)):                # on vérifie qu'il y a assez de places pour les cases à insérer
            if [i,y] not in non_empties_coordinates :
                empty_tiles = empty_tiles + 1
        if not(empty_tiles < len(liste)):  # s'il y a assez de places

            k = x
            j = 0
            while j < len(liste) and neighbours:
                if [k,y] not in non_empties_coordinates :
                    back_up[k][y] = liste[j]
                    neighbours = neighbours and neighbour(grid,k,y,liste[j],direction)
                    j = j + 1

                k = k + 1

            k = x
            while ( k>-1) and (back_up[k][y] != " "): # on retourne en arrière pour avoir la formule complète
                formule =  back_up[k][y] + formule
                k= k - 1
            xi = k + 1 #xinitial de la formule
            yi = y  # yinitial de la formule
            k = x+1
            while(k< len(grid))and (back_up[k][y] != " "):
                formule = formule + back_up[k][y]
                k= k + 1
    if direction == "droite":
        empty_tiles = 0
        for i in range(y,len(grid)):
            if [x,i] not in non_empties_coordinates :
                empty_tiles = empty_tiles + 1
        if not(empty_tiles < len(liste)):

            k = y
            j = 0
            while j < len(liste) and neighbours :
                if [x,k] not in non_empties_coordinates :
                    back_up[x][k] = liste[j]
                    neighbours = neighbours and neighbour(grid,x,k,liste[j],direction)
                    j = j + 1
                k = k + 1
            k = y
            while ( k>-1) and (back_up[x][k] != " "):
                formule =  back_up[x][k] + formule
                k= k - 1
            xi = x #xinitial de la formule
            yi = k + 1  # yinitial de la formule

            k = y+1
            while(k< len(grid))and (back_up[x][k] != " "):
                formule = formule + back_up[x][k]
                k= k + 1
    print("formule:",  formule)
    position_valide = len(formule) != len(liste)
    if verification_operation(formule) == True and position_valide and neighbours :
        grid = back_up
        return(xi,yi,grid,formule)
    return(-1,-1,grid,"erreur")


def get_stock():
    
    """ Cette fonction retourne le stock """
    
    global stock
    return(stock)


def initiate_stock():
    
    """ Cette fonction initialise le stock""" 
    global stock
    stock = ['0', '0', '0', '0', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '1', '2', '4', '5', '6', '8', '9', '3', '3', '3', '3', '3', '3', '7', '7', '7', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '/', '/', '/', '/', '*', '*', '*', '*', '*', '*', '=', '+', '=', '+', '=', '-', '=', '-', '=', '-', '=', '+', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', 'joker', 'joker', 'joker', 'joker']

def modify_stock(st):
    
    """ Cette fonction permet de regénérer un stock  """
    global stock
    stock = st
