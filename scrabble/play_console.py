

#***************************************************************** Importations ****************************************************************

from scrabble.grid import *

#*************************************************************** Initialisations ***************************************************************

# definition d'un stock 
stock = get_stock()


#initialisation de la main 
hand =[]
pioche (hand)

# initialisation du score à 0
score = 0

# generation d'une grille avec des cases aléatoires
grid = generate_grid_with_random_elements()



# affichage de la grille 
for i in range(15):
    print(grid[i])




def play_a_turn():
    
    """ Cette fonction permet au joueur de jouer à un tour """
    
    global stock
    global hand
    global score,grid
    hand_copy = hand.copy()
    operation=[]
    print(hand)

    tour = input("Would you like to change some pieces ? Y / N ")   # Le joueur a le choix de changer des pièces de sa main 
    if tour.upper() in ["Y","N"] and tour.upper() == "N":
        while True :
            piece=select_piece(hand_copy)
            operation.append(piece)
            test=submit_test()
            if test or len(hand)==0 :
                break
        x = int(input("entrez ligne"))  
        y = int(input("entrez colonne"))
        direction = input("entrez direction")
        grid_copy = grid.copy()
        xi,yi,grid,formule = position(x,y,direction,operation,grid)
        if grid != grid_copy:
            score = score + evaluate(xi,yi,direction,operation,grid_copy)   # calcul du score lors de chaque tour
            hand = hand_copy
            for i in range(len(operation)):
                hand.append(generate_piece())
        else :
            print("commande invalide")
        for i in range(15):
            print(grid[i])
    else :
        nbre = int(input("Nombre de pièces à relaner, 0 pour passer son tour"))
        for i in range(nbre) :
            stock.append(select_piece(hand))
        for i in range(nbre):
            hand.append(generate_piece())


# Lancer la boucle du  jeu 

while True :
    play_a_turn()
